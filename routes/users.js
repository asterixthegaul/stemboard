const express = require('express');
const router = express.Router();
const passport = require('passport');
const { ensureAuthenticated } = require('../config/auth');
const bcrypt = require('bcryptjs');

router.get('/login', (req, res) => {
    // Render login page if user is not logged in, otherwise redirect to home page.
    if (!req.user) {
        res.render('login', {
            pageTitle: 'Login | STEM Akademija Board',
            cssLinks: [
                {link: '/css/signin.css'}
            ],
            centerText: true,
            currentYear: (new Date()).getFullYear()
        });
    } else {
        res.redirect('/');
    }
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
       successRedirect: '/',
       failureRedirect: 'login',
       failureFlash: true 
    })(req, res, next);
});

router.get('/profile', ensureAuthenticated, (req, res) => {
    res.render('profile', {
        user: req.user,
        pageTitle: 'Profile | STEM Akademija',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ]
    });
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('login');
});

router.post('/update', ensureAuthenticated, async (req, res) => {
    let { email, mobilePhoneNumber, currentPassword, newPassword, confirmNewPassword } = req.body;

    // Based on what user has changed (updated), store in array confirmation messages or error
    // that can be rendered at profile page.
    const successUpdates = [];
    const errorUpdates = [];

    if (mobilePhoneNumber != '' && mobilePhoneNumber != 'Not Set' && mobilePhoneNumber != req.user.mobilePhoneNumber) {
        // Verify if phone number in given format:
        // +387 00 000 000
        mobilePhoneNumber = mobilePhoneNumber.match(/^\+\d{3}\s\d{2}\s(\d{3}|\d{2})\s\d{3}/g);
        if (!mobilePhoneNumber)
        {
            errorUpdates.push({msg: 'Please provide phone number in format +387 00 000 000'});
        } else {
            successUpdates.push({msg: 'Your phone number is changed.'});
            req.user.mobilePhoneNumber = mobilePhoneNumber[0];
        }
    }

    if (currentPassword != '') {
        // New password can be set only if the currentPassword field is same as from the account
        const success = await bcrypt.compare(currentPassword, req.user.password);

        // If the password matches the account password then update password with the new one
        // Password needs to have >= 6 characters
        if (success) {
            if (newPassword != '' && newPassword.length >= 6) {
                if (newPassword === confirmNewPassword) {
                    const salt = await bcrypt.genSalt(10);
                    const hash = await bcrypt.hash(newPassword, salt);
                    req.user.password = hash;
                    successUpdates.push({msg: 'The password has been successfully changed.'});
                } else {
                    errorUpdates.push({msg: 'The passwords do not match.'});
                }
            } else {
                errorUpdates.push({msg: 'The password must be at least 6 characters long.'});
            }
        } else {
            errorUpdates.push({msg: 'Current password does not match with your account.'});
        }
    }

    if (errorUpdates.length > 0) {
        req.flash('updateErrors_msgs', errorUpdates);
    }

    if (successUpdates.length > 0) {
        await req.user.save();
        req.flash('updateSuccess_msgs', successUpdates);
    }

    res.redirect('profile');
});

module.exports = router;