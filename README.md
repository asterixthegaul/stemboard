# STEM Board

STEM Akademija Board is an application for dealing with management of students, classrooms, their attendance and simple financial overview for current month.

## Installation

1. Clone repository: `git clone https://bitbucket.org/asterixthegaul/stemboard.git`
1. Install dependencies: `npm install`
1. Since it is using MongoDB Atlas, you have to create in folder `config` file `keys.js` where is connection string for MongoDB Atlas located:

    ```javascript
    module.exports = {
        mongoAtlasSRV: ''
    };
    ```

1. You can insert admin through MongoDB Atlas collection interface or you can create file `insert_admin.js` (or any name you choose) to insert user (as admin) when command `node insert_admin` runs in the terminal:

    ```javascript
    const User = require('./models/User');
    const mongoose = require('mongoose');
    const bcrypt = require('bcryptjs');

    const admin = {
        firstName: '',
        lastName: '',
        email: '',
        title: 'Lecturer and Assistant',
        password: '',
        birthDate: new Date(1995, 12, 1), // put your birth date here
        accountType: 2 // account type of 2 means it has admin privileges 
    };

    const db = require('./config/keys').mongoAtlasSRV;

    mongoose.connect(db, {useNewUrlParser: true})
    .then(() => {
        User.findOne({accountType: 2})
        .then((user) => {
            if (user) {
                console.log('Admin already exist in database!');
            } else {
                bcrypt.genSalt(10, (err, salt) => {
                    if (err) throw err;

                    bcrypt.hash(admin.password, salt, (err, hash) => {
                        if (err) throw err;

                        admin.password = hash;
                        const newAdmin = new User(admin);
                        newAdmin.save()
                        .then(() => { 
                            console.log('Admin is created!')
                        })
                        .catch((err) => {
                            throw err;
                        });
                    });
                });
            }
        })
        .catch((err) => {
            throw err;
        });
    })
    .catch((err) => {
        throw err;
    });
    ```

> There can only be one administrator account.
