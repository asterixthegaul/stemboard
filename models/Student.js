const mongoose = require('mongoose');

const StudentSchema = mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    birthDate: {
        type: Date,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    firstParentFName: {
        type: String,
        required: true
    },
    firstParentLName: {
        type: String,
        required: true
    },
    firstParentMobilePhoneNumber: {
        type: String,
        required: true,
    },
    secondParentFName: {
        type: String,
        default: ''
    },
    secondParentLName: {
        type: String,
        default: ''
    },
    secondParentMobilePhoneNumber: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true,
    },
    stemClass: {
        type: String,
        required: true
    },
    registeredOn: {
        type: Date,
        default: Date.now()
    }
});

const Student = mongoose.model('Student', StudentSchema);
module.exports = Student;