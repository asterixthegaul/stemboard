const express = require('express');
const router = express.Router();
const ensureAuthenticated = require('../config/auth').ensureAuthenticated;
const ensureAuthenticatedAndAdmin = require('../config/auth').ensureAuthenticatedAndAdmin;
const Student = require('../models/Student');
const Payment = require('../models/Payment');

router.get('/', ensureAuthenticated, async (req, res) => {
    // Get total students and how many of them are males and females
    let totalStudents, femaleStudents, maleStudents;

    // All payments from collection but take sum only for the current month
    let payments;

    try {
        totalStudents = await Student.countDocuments();
        femaleStudents = await Student.find({gender: 'Female'});
        maleStudents = await Student.find({gender: 'Male'});
        payments = await Payment.find({});
    } catch (err) {
        throw (err);
    }

    // Calculate earnings only for current month
    let currentTotalPayments = 0;
    for (let i = 0; i < payments.length; ++i) {
        // Get only month from the date which after spliting is 2nd element in array
        if (payments[i].datePaid.toString().split(' ').splice(0, 4)[1] === (new Date()).toString().split(' ').splice(0, 4)[1]) {
            currentTotalPayments += payments[i].amount;
        }
    }

    res.render('index', {
        user: req.user,
        pageTitle: 'Board | STEM Akademija',
        cssLinks: [
            {link: '/css/dashboard.css'},
        ],
        totalStudents,
        femaleStudents: femaleStudents.length,
        maleStudents: maleStudents.length,
        currentTotalPayments
    });
});

router.get('/earnings', ensureAuthenticatedAndAdmin, async (req, res) => {
    const payments = await Payment.find({});
    res.json(payments);
});

module.exports = router;