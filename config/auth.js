module.exports = {
    ensureAuthenticated: (req, res, next) => {
        if (req.isAuthenticated()) {
            return next();
        }

        res.redirect('/users/login');
    },
    ensureAuthenticatedAndAdmin: (req, res, next) => {
        if (req.isAuthenticated() && req.user.accountType === 2) {
            return next();
        }

        req.flash('error', 'You are not allowed to insert new students.')
        res.redirect('/');
    }
};