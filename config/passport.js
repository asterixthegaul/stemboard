const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const User = require('../models/User');

module.exports = (passport) => {
    passport.use(
        new LocalStrategy({usernameField: 'inputEmail', passwordField: 'inputPassword'}, (email, password, done) => {
            User.findOne({email: email}).then((user) => {
                // Email is not registered on any user
                if (!user) {
                    return done(null, false, {message: 'Please provide a valid email and password.'})
                }

                bcrypt.compare(password, user.password, (err, success) => {
                    if (err) throw err;

                    // On success it returns user otherwise passwords do not match
                    if (success) {
                        return done(null, user);
                    } else {
                        return done(null, false, {message: 'Please provide a valid email and password.'})
                    }
                });
            }).catch((err) => {
                throw err;
            });
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });
};