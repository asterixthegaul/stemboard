const mongoose = require('mongoose')

const ClassSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    year: {
        type: String,
        required: true
    },
    students: {
        type: Array,
        default: []
    },
    generatedOn: {
        type: Date,
        default: Date.now()
    }
});

const Class = new mongoose.model('Class', ClassSchema);
module.exports = Class;