const express = require('express');
const expressLayout = require('express-ejs-layouts');
const session = require('express-session');
const flash = require('connect-flash');
const mongoose = require('mongoose');
const path = require('path');
const passport = require('passport');

const app = express();
require('./config/passport')(passport);
const PORT = process.env.PORT || 5000;

const db = require('./config/keys').mongoAtlasSRV;

mongoose.connect(db, {useNewUrlParser: true})
.then(() => {
    console.log('Successfully connected to MongoDB Atlas!');
})
.catch((err) => {
    throw err;
});

app.use(express.static(path.join(__dirname, 'public')));

// Layout for express based on EJS
app.use(expressLayout);
app.set('view engine', 'ejs');

// Middleware for easy parsing data
app.use(express.urlencoded({extended: false}));

// Session middleware
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
}));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Flash middleware
app.use(flash());

// Defining global veriales
app.use((req, res, next) => {
    res.locals.error_msg = req.flash('error_msg');
    res.locals.updateSuccess_msgs = req.flash('updateSuccess_msgs');
    res.locals.updateErrors_msgs = req.flash('updateErrors_msgs');
    res.locals.error = req.flash('error');
    next();
});

// Route paths for users
app.use('/users', require('./routes/users'));

// Root path
app.use('/', require('./routes/index'));

// Route paths for students
app.use('/students', require('./routes/students'));

app.listen(PORT, () => {
    console.log(`Server started on port: ${PORT}`);
});