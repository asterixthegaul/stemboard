const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    mobilePhoneNumber: {
        type: String,
        default: ' '
    },
    title: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    registeredOn: {
        type: Date,
        default: Date.now()
    },
    birthDate: {
        type: Date,
        required: true,
    },
    accountType: {
        type: Number,
        default: 0
    }
});

const User = new mongoose.model('User', UserSchema);
module.exports = User;