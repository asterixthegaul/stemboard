const mongoose = require('mongoose');

const AttendanceScheme = new mongoose.Schema({
    stemClass: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    studentsAttending: {
        type: Array,
        default: []
    },
    dateCreated: {
        type: Date,
        default: Date.now()
    }
});

const Attendance = mongoose.model('Attendance', AttendanceScheme);
module.exports = Attendance;