const express = require('express');
const router = express.Router();
const ensureAuthenticatedAndAdmin  = require('../config/auth').ensureAuthenticatedAndAdmin;
const Class = require('../models/Class');
const Student = require('../models/Student');
const Attendance = require('../models/Attendance');
const Payment = require('../models/Payment');
const mongoose = require('mongoose');

router.get('/insert', ensureAuthenticatedAndAdmin, async (req, res) => {
    let allClasses;
    try {
        allClasses = await Class.find({});  
    } catch (err) {
        throw err;
    }

    res.render('insert_student', {
        user: req.user,
        pageTitle: 'Insert Student | STEM Akademija Board',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ],
        classes: allClasses
    });
});

router.post('/insert', ensureAuthenticatedAndAdmin, async (req, res) => {
    // Is there a better way to do this? Yes, first version is always funny one.
    let { 
        newStudentFirstName, newStudentLastName, newStudentBirthDate, newStudentGender,
        newStudentFParentFirstName, newStudentFParentLastName, newStudentFPMobilePhoneNumber,
        newStudentSParentFirstName, newStudentSParentLastName, newStudentSPMobilePhoneNumber,
        newStudentAddress, newStudentCity, newStudentClass
    } = req.body;

    const successUpdates = [];
    const errorUpdates = [];

    if (!newStudentFirstName || !newStudentLastName || !newStudentBirthDate || !newStudentGender
    || !newStudentFParentFirstName || !newStudentFParentLastName || !newStudentFPMobilePhoneNumber
    || !newStudentAddress || !newStudentCity) {
        errorUpdates.push({msg: 'All fields are important besides second parent.'});
    }

    if (newStudentFPMobilePhoneNumber) {
        newStudentFPMobilePhoneNumber = newStudentFPMobilePhoneNumber.match(/^\+\d{3}\s\d{2}\s(\d{3}|\d{2})\s\d{3}/g);
        if (!newStudentFPMobilePhoneNumber)
        {
            errorUpdates.push({msg: 'Please provide phone number for first parent in format +387 00 000 000'});
        }
    }

    if (newStudentSPMobilePhoneNumber && newStudentSParentFirstName) {
        newStudentSPMobilePhoneNumber = newStudentSPMobilePhoneNumber.match(/^\+\d{3}\s\d{2}\s(\d{3}|\d{2})\s\d{3}/g);
        if (!newStudentSPMobilePhoneNumber)
        {
            errorUpdates.push({msg: 'Please provide phone number for second parent in format +387 00 000 000'});
        }
    }

    if (newStudentFParentFirstName === newStudentSParentFirstName) {
        errorUpdates.push({msg: 'Please provide different first names for first and second parent or leave empty fields for second parent.'});
    }
 
    if (errorUpdates.length > 0) {
        let allClasses;
        try {
            allClasses = await Class.find({});  
        } catch (err) {
            throw err;
        }

        res.render('insert_student', {
            user: req.user,
            errors: errorUpdates,
            pageTitle: 'Insert Student | STEM Akademija Board',
            cssLinks: [
                {link: '/css/dashboard.css'}
            ],
            classes: allClasses,
            newStudentFirstName, newStudentLastName, newStudentBirthDate,
            newStudentFParentFirstName, newStudentFParentLastName, newStudentFPMobilePhoneNumber,
            newStudentSParentFirstName, newStudentSParentLastName, newStudentSPMobilePhoneNumber,
            newStudentAddress, newStudentCity
        });
        return;
    } 

    // Since date is picked from input of type date, it is in form YYYY-MM-DD
    // After spliting, constructing new Date() based on indexes in the array and casting it to number
    newStudentBirthDate = newStudentBirthDate.split('-');
    const newStudent = Student({
        firstName: newStudentFirstName,
        lastName: newStudentLastName,
        birthDate: new Date(Number(newStudentBirthDate[0]), Number(newStudentBirthDate[1]), Number(newStudentBirthDate[2])),
        gender: newStudentGender,
        firstParentFName: newStudentFParentFirstName,
        firstParentLName: newStudentFParentLastName,
        firstParentMobilePhoneNumber: newStudentFPMobilePhoneNumber,
        secondParentFName: newStudentSParentFirstName,
        secondParentLName: newStudentSParentLastName,
        secondParentMobilePhoneNumber: newStudentSPMobilePhoneNumber,
        address: newStudentAddress,
        city: newStudentCity,
        stemClass: newStudentClass
    });

    await newStudent.save();
    successUpdates.push({msg: 'New student successfully created.'});
    req.flash('updateSuccess_msgs', successUpdates);

    // Since student is added, we need to update document that holds IDs of students
    await Class.updateOne({name: newStudentClass}, {$push: {students: newStudent._id}});

    res.redirect('insert');
});

router.get('/insert/class', ensureAuthenticatedAndAdmin, async (req, res) => {
    let allClassses, studentsWithoutClassroom;
    
    const alerts = [];

    try {
        allClassses = await Class.find({});
        studentsWithoutClassroom = await Student.find({stemClass: 'None'});
    } catch (err) {
        throw err;
    }

    if (studentsWithoutClassroom.length > 0) {
        alerts.push({msg: 'There are some students that do not have a classroom assigned. Click <a href="none">here</a> to add them.'});
    }

    // Create dictionary of students to be correctly referenced in a table for a given class
    let allStudents = await Student.find({});
    let allStudentsDict = {};
    allStudents.forEach((student) => {
        allStudentsDict[student._id] = student;
    });

    res.render('insert_class', {
        user: req.user,
        pageTitle: 'Insert Class | STEM Akademija Board',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ],
        currentYear: (new Date()).getFullYear(),
        classes: allClassses,
        students: allStudentsDict,
        alerts: alerts
    });
});

router.get('/class/remove', ensureAuthenticatedAndAdmin, async (req, res) => {
    const id = req.query.id;
    
    if (mongoose.Types.ObjectId.isValid(id))
    {
        const successUpdates = [];

        const classToRemove = await Class.findOne({_id: id});

        // Go through each student in a class and remove assigned classroom to their document
        for (let i = 0; i < classToRemove.students.length; ++i) {
            let student = await Student.findOne({_id: classToRemove.students[i]});
            student.stemClass = 'None';
            await student.save();
        }

        const name = classToRemove.name;

        await classToRemove.remove();
        successUpdates.push({msg: 'The class ' + name + ' has been successfully removed.'});
        req.flash('updateSuccess_msgs', successUpdates);
    }

    res.redirect('/students/insert/class');
});

router.post('/insert/class', ensureAuthenticatedAndAdmin, async (req, res) => {
    const { newClassName } = req.body;

    const successUpdates = [];
    const errorUpdates = [];
    let newClass;

    if (newClassName == '') {
        errorUpdates.push({msg: 'Please fill in class name.'});
    } else {
        try {
            const success = await Class.findOne({name: newClassName});

            // Class name does not exist
            if (!success)
            {
                successUpdates.push({msg: 'A new class name has been generated.'});
                const newClassYear = (new Date()).getFullYear();
                newClass = new Class({
                    name: newClassName,
                    year: newClassYear
                });
            }
            else 
            {
                errorUpdates.push({msg: 'Class name already exists.'});
            }
        } catch (err) {
            throw err;
        }
    }

    if (errorUpdates.length > 0) {
        req.flash('updateErrors_msgs', errorUpdates);
    } else {
        await newClass.save();
        req.flash('updateSuccess_msgs', successUpdates);
    }

    res.redirect('class');
});

router.get('/remove', ensureAuthenticatedAndAdmin, async (req, res) => {
    const id = req.query.id;

    // Remove student from the class if the student ID is valid.
    // Since there is document that holds for students classrooms, we have to remove from the array 
    // student id.
    if (mongoose.Types.ObjectId.isValid(id)) {
        let student = await Student.findOne({_id: id});
        let studentIndex, classroom;

        if (student.stemClass != 'None') {
            classroom = await Class.find({name: student.stemClass});
            for (let i = 0; i < classroom.length; i++) {
                
                studentIndex = classroom[i].students.indexOf(student._id);

                if (studentIndex > -1) {
                    classroom[i].students.splice(studentIndex, 1);
                    await classroom[i].save();
                    break;
                }
            }

            student.stemClass = 'None';
            await student.save();

            req.flash('updateSuccess_msgs', {msg: 'The student was removed from the classroom.'});
        } else {
            req.flash('updateErrors_msgs', {msg: 'The students has been already removed from one of the classrooms.'});
        }
    } else {
        req.flash('updateErrors_msgs', {msg: 'Invalid student ID. Failed to remove from classroom.'});
    }

    res.redirect('insert/class');
});

router.get('/insert/none', ensureAuthenticatedAndAdmin, async (req, res) => {
    let missingStudents, allClasses;

    try {
        missingStudents = await Student.find({stemClass: 'None'});
        allClasses = await Class.find({});  
    } catch (err) {
        throw err;
    }

    if (missingStudents.length > 0) {
        res.render('insert_none', {
            user: req.user,
            pageTitle: 'Insert Missing | STEM Akademija Board',
            cssLinks: [
                {link: '/css/dashboard.css'}
            ],
            students: missingStudents,
            classes: allClasses
        });
    } else {
        res.redirect('class');
    }
});

router.get('/move', ensureAuthenticatedAndAdmin, async (req, res) => {
    const id = req.query.id;
    const classroom = req.query.classroom;

    if (mongoose.Types.ObjectId.isValid(id)) {
        let student, stemClass;
        try {
            student = await Student.findOne({_id: id});
            stemClass = await Class.findOne({_id: classroom});

            student.stemClass = stemClass.name;
            stemClass.students.push(student._id);

            req.flash('updateSuccess_msgs', {msg: 'Student ' + student.firstName + ' ' + student.lastName + ' is moved to ' + stemClass.name + ' classroom.'});            
            await student.save();
            await stemClass.save();
        } catch (err) {
            throw err;
        }
    } else {
        req.flash('updateErrors_msgs', {msg: 'Invalid student ID. Failed to move to classroom.'});
    }

    res.redirect('insert/none');
});

router.get('/attendance', ensureAuthenticatedAndAdmin, async (req, res) => {
    let classrooms, students;

    try {
        classrooms = await Class.find({});
        students = await Student.find({});
    } catch (err) {
        throw err;
    }

    // Array holding classrooms for which attendance is not created on current date
    const attendanceClassrooms = [];
    const presence = [];

    // In order to get both classrooms that are not generated for current date and classrooms with current date
    // where we have data for presence of students we have to go through all classrooms present
    for (let i = 0; i < classrooms.length; ++i) {
        const attendance = await Attendance.find({stemClass: classrooms[i]._id});

        // We will assume at the beginning that given classroom for current date does not exist in Attendance
        // collection and try to compare each date in order to find one
        let exists = false, j = 0;
        for (; j < attendance.length; ++j) {
            if (attendance[j].dateCreated.toString().split(' ').slice(1, 4).join(' ') === (new Date()).toString().split(' ').slice(1, 4).join(' ')) {
                exists = true;
                break;    
            }
        }

        // If the classroom for current date does not exists push it to the attendance to allow generation of
        // attendance otherwise, get all students with the same classroom and push information to presence
        // where it will be used for table as viewing students and marking who is present
        if (!exists) {
            attendanceClassrooms.push({id: classrooms[i]._id, name: classrooms[i].name});
        } else {
            const studentsForGivenClassroom = await Student.find({stemClass: classrooms[i].name});
            presence.push({id: classrooms[i]._id, name: classrooms[i].name, year: classrooms[i].year, studentsAttending: attendance[j].studentsAttending, students: studentsForGivenClassroom});
        }
    }

    res.render('insert_attendance', {
        user: req.user,
        pageTitle: 'Add Attendance | STEM Akademija Board',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ],
        classroomsLength: classrooms.length,
        studentsLength: students.length,
        currentDate: (new Date()).toString().split(' ').slice(1, 4).join(' '),
        attendanceClassrooms,
        presence
    });
});

router.post('/attendance/insert', ensureAuthenticatedAndAdmin, async (req, res) => {
    const { newAttendanceClassroom } = req.body;

    const newAttendance = Attendance({
        stemClass: newAttendanceClassroom
    });

    await newAttendance.save();

    req.flash('updateSuccess_msgs', {msg: 'Attendance was successfully generated.'});
    res.redirect('/students/attendance');
});

router.post('/attendance', ensureAuthenticatedAndAdmin, async (req, res) => {
    let { presence } = req.body;
    const studentsMarked = [];

    // When only one checkbox is selected, type of presence will be string
    // split string to make array, because of using array indexing in queries below
    if (typeof presence === 'string') {
        presence = presence.split();
    }

    // Find attendance id by the first user (since they are all present in the same classroom)
    let student;
    
    if (presence[0].includes('dummy'))
        student = await Student.findOne({_id: presence[0].split('dummy')[0]});
    else
        student = await Student.findOne({_id: presence[0]});

    const stemClass = await Class.findOne({name: student.stemClass});

    // Since there can be multiple documents with same ID of classroom, we have to find one which has current date
    let attendance = await Attendance.find({stemClass: stemClass._id});
    for (let i = 0; i < attendance.length; ++i) {
        if (attendance[i].dateCreated.toString().split(' ').slice(1, 4).join(' ') == (new Date()).toString().split(' ').slice(1, 4).join(' ')) {
            attendance = attendance[i];
            break;
        }
    }

    // Since there is "dummy" as first student ID, if we don't select any student from the list
    // it means we want to remove all students in classroom
    if (presence.length == 1) {
        attendance.studentsAttending.splice(0, attendance.studentsAttending.length);
    } else {
        for (let i = 0; i < stemClass.students.length; ++i) {
            // If the student is present in presence array it can mean one of two things:
            // 1. student does not exist in attendance array, push him
            // 2. student exist in attendance array, leave him
            if (presence.indexOf(stemClass.students[i].toString()) !== -1) {
                if (attendance.studentsAttending.indexOf(stemClass.students[i]) === -1) {
                    attendance.studentsAttending.push(stemClass.students[i]);
                }
            } else {
                // We have to make sure that student is inserted in the array, if he is not
                // we will remove wrong student
                const indexToRemove = attendance.studentsAttending.indexOf(stemClass.students[i]);

                if (indexToRemove !== -1) {
                    attendance.studentsAttending.splice(indexToRemove, 1);
                }
            }
        }
    }

    await attendance.save();
    res.redirect('/students/attendance');
});

router.get('/attendance/history', ensureAuthenticatedAndAdmin, async (req, res) => {
    
    let attendance;
    try {
        attendance = await Attendance.find({});
    } catch (err) {
        throw err;
    }

    // Presence of students will be based on the stem classroom + students that are assigned
    // to present classroom.
    let presence = [];
    for (let i = 0; i < attendance.length; ++i) {
        const classroom = await Class.findOne({_id: attendance[i].stemClass});
        const studentsForGivenClassroom = await Student.find({stemClass: classroom.name});

        presence.push({id: classroom._id + i, name: classroom.name, year: classroom.year, dateCreated: attendance[i].dateCreated, studentsAttending: attendance[i].studentsAttending, students: studentsForGivenClassroom});
    }

    res.render('attendance_history', {
        user: req.user,
        pageTitle: 'Attendance History | STEM Akademija Board',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ],
        presence
    });
});

router.get('/payments', ensureAuthenticatedAndAdmin, async (req, res) => {
    res.render('insert_payment', {
        user: req.user,
        pageTitle: 'Payments | STEM Akademija Board',
        cssLinks: [
            {link: '/css/dashboard.css'}
        ]
    });
});

router.get('/payments/search', ensureAuthenticatedAndAdmin, async (req, res) => {
    let { firstName, lastName } = req.query;

    // Searching for students based on parent first and last name (TODO: add for second parent)
    // if the firstName or lastName query parameter is empty it will return all students since
    // we are querying using OR operator.
    if (firstName === '')
        firstName = '-';
    if (lastName === '')
        lastName = '-';
    
    // Students whose parent first and last name contains characters from firstName and lastName variable
    let student = await Student.find({$or: [{firstParentFName: {$regex: '^' + firstName, $options: 'i'}}, {firstParentLName: {$regex: '^' + lastName}}]});

    // Get all payments for student(s) in a list if student(s) exist and insert in a new field payments
    if (student.length > 0)
    {
        for (let i = 0; i < student.length; ++i) {
            // Create field payments, which is an empty array
            const payment = await Payment.find({student: student[i]._id});

            // Since mongoose document permits adding new properties to object (based on schema), we have to convert
            // it to object
            student[i] = student[i].toObject();
            student[i].payments = [];

            // Only if the payment date is current month add it to list
            for (let j = 0; j < payment.length; ++j) {
                if (payment[j].datePaid.toString().split(' ').slice(1, 2).join(' ') === (new Date()).toString().split(' ').slice(1, 2).join(' ')) {
                    student[i].payments.push({datePaid: payment[j].datePaid.toString().split(' ').slice(1, 4).join(' '), amount: payment[j].amount});
                }
            }
        }
    }

    res.json(student);
});

router.post('/payments/save', ensureAuthenticatedAndAdmin, async (req, res) => {
    const { newStudentPaymentAmount, newStudentIDPayment } = req.body;

    try {
        const newPayment = Payment({
            student: newStudentIDPayment,
            amount: newStudentPaymentAmount
        });

        await newPayment.save();
        const student = await Student.findOne({_id: newStudentIDPayment});

        req.flash('updateSuccess_msgs', {msg: `Payment for student ${student.firstName} ${student.lastName} confirmed!`});
    } catch (err) {
        throw err;
    }

    res.redirect('/students/payments');
});

module.exports = router;