const mongoose = require('mongoose');
const PaymentScheme = mongoose.Schema({
    student: {
        type: String,
        required: true
    },
    datePaid: {
        type: Date,
        default: Date.now()
    },
    amount: {
        type: Number,
        required: true
    }
});

const Payment = new mongoose.model('Payment', PaymentScheme);
module.exports = Payment;